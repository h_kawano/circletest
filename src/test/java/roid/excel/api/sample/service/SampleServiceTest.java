package roid.excel.api.sample.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest
class SampleServiceTest {
    @Autowired
    private SampleService sampleService;

    @Test
    void addOneToString() {
        String result = sampleService.addOneToString("3");
        assertEquals("4",result);
    }

    @Test
    void addDefToString() {
        String result = sampleService.addDefToString("abc");
        assertEquals("abcdef",result);
    }

    @Test
    void updateSampleRequestModel() {
    }
}