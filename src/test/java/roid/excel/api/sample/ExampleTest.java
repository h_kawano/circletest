package roid.excel.api.sample;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExampleTest {

    @Test
    void divide5_divisible() {
    	System.out.println("test desu");
        Example example = new Example();
        example.checkDevide5(25);
        boolean result = example.devide5(25);
        assertEquals(true, result);
    }

    @Test
    void divide5_notDivisible() {
        System.out.println("test dayo dayo");
        Example example = new Example();
        example.checkDevide5(25);
        boolean result = example.devide5(18);
        assertEquals(false, result);
    }
}