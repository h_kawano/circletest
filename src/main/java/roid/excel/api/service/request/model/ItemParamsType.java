package roid.excel.api.service.request.model;



/**
 * ItemParamsのタイプを指定
 */
public enum ItemParamsType {

    STRING,
    NUMERIC,
    DATE,
    IMAGEPATH
    ;

}
