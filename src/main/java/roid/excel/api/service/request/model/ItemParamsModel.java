package roid.excel.api.service.request.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;


/**
 *  itemParamsのvalueモデル
 */
@JsonDeserialize(builder = ItemParamsModel.ItemParamsModelBuilder.class)
public class ItemParamsModel {

    private ItemParamsType type;
    private String key;
    private String item;

    public ItemParamsType getType() {
        return type;
    }

    public void setType(ItemParamsType type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class ItemParamsModelBuilder {

    }
}
