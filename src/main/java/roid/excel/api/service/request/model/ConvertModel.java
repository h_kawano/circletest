package roid.excel.api.service.request.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.List;

/**
 * エクセル変換APIにリクエストされるモデル
 */
@JsonDeserialize(builder = ConvertModel.ConvertModelBuilder.class)
public class ConvertModel {

    private String inputFileName;
    private String outputFileName;
    private List<ItemParamsModel> itemParams;

    public String getInputFileName() {
        return inputFileName;
    }

    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public void setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

    public List<ItemParamsModel> getItemParams() {
        return itemParams;
    }

    public void setItemParams(List<ItemParamsModel> itemParams) {
        this.itemParams = itemParams;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class ConvertModelBuilder {

    }
}
