package roid.excel.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import roid.excel.api.resource.ResourceAccessor;
import roid.excel.api.service.request.model.ConvertModel;

import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * エクセル変換API用Service
 */
@Service
public class ExcelConvertService {

    @Autowired
    private ResourceAccessor resourceAccessor;

    /**
     * ResourceAccerssorクラスのgetTemporaryFileメソッドを利用
     *
     * @param convertModel レクエストされたjsonモデル
     * @param tmpDir Fileオブジェクト保存用の一時ディレクトリ
     * @return Map<String,String>
     */
    public boolean convert(ConvertModel convertModel,Path tmpDir) {

        // ファイルパスを取得
        String filePath = convertModel.getInputFileName();

        // ファイルを取得
        File file = resourceAccessor.getTemporaryFile(filePath,tmpDir);

        return file != null;

        /**
         * エクセルファイルへのパラメータの埋め込み
         *
         */


        /**
         * パラメータを埋め込んだエクセルファイルを保存する
         */
    }

}
