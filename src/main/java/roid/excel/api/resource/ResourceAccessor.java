package roid.excel.api.resource;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.WritableResource;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/**
 * エクセル変換API用
 * リソースアクセス部品
 */
@Component
public class ResourceAccessor {

    private final ResourceLoader resourceLoader;

    @Value("${app.resource.config.root}")
    private String root;

    public ResourceAccessor(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    /**
     * 指定したpathからファイルをダウンロードし、一時ディレクトリ配下にファイルオブジェクトとして作成の上返却します。
     * pathはapplication.yml定義のrootからのpathを指定してください。
     * 取得失敗時やファイルが存在しない場合、nullを返却します。
     *
     * @param filePath rootからのpath
     * @param tmpDir Fileオブジェクト保存用の一時ディレクトリ
     * @return File 取得できない場合はnull
     */
    public File getTemporaryFile(String filePath, Path tmpDir) {
        try {
            Resource resource = this.resourceLoader.getResource(this.root + filePath);
            if (resource.exists()) {
                byte[] content = FileCopyUtils.copyToByteArray(resource.getInputStream());
                File tmpFile = tmpDir.resolve(Objects.requireNonNull(resource.getFilename())).toFile();
                FileCopyUtils.copy(content, tmpFile);
                return tmpFile;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 指定ファイルを指定pathに保存します。
     * pathはapplication.yml定義のrootからのpathを指定してください。
     *
     * @param file     保存するファイル
     * @param filePath rootからのpath
     * @return boolean 保存成功時：true、保存失敗時：false
     */
    public boolean saveFile(File file, String filePath) {
        WritableResource resource = (WritableResource) this.resourceLoader.getResource(this.root + filePath);
        if (resource instanceof FileUrlResource) {
            mkdirs((FileUrlResource) resource);
        }
        try {
            FileCopyUtils.copy(Files.readAllBytes(file.toPath()), resource.getOutputStream());
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private void mkdirs(FileUrlResource resource) {
        try {
            File parentDir = resource.getFile().getParentFile();
            if (parentDir != null && !parentDir.exists()) {
                parentDir.mkdirs();
            }
        } catch (IOException ignored) {
        }
    }
}
