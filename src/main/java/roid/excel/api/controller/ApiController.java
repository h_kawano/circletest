package roid.excel.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import roid.excel.api.service.ExcelConvertService;
import roid.excel.api.service.request.model.ConvertModel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;


/**
 * エクセル変換API用のControllerクラス
 */
@RestController
@RequestMapping(value = "/api/excel")
public class ApiController {

    @Autowired
    ExcelConvertService excelConvertService;

    /**
     * エクセル変換APIにPOSTすると
     * レスポンスを返す
     *
     * @param convertModel リクエスト時に送られて来るjsonモデル
     * @return ResponseEntity<String>
     */
    @RequestMapping(method = RequestMethod.POST, value = "convert")
    public ResponseEntity<Map<String, String>> convert(ConvertModel convertModel) throws IOException {

        // ファイルをダウンロードする位置時ディレクトリの作成
        Path tmpDir = Files.createTempDirectory("");


        try {
            boolean result = excelConvertService.convert(convertModel, tmpDir);
            Map<String,String> message = new HashMap<>();

            if (result) {
                message.put("test", "file == null");
            } else {
                    message.put("test", "file != null");
            }

            return new ResponseEntity<Map<String, String>>(message, HttpStatus.OK);

        } finally {
                // 一時ディレクトリを削除
                FileSystemUtils.deleteRecursively(tmpDir);
        }
    }
}
