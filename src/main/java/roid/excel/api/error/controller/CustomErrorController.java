package roid.excel.api.error.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import roid.excel.api.error.response.model.ErrorResponseModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * エラー用コントローラクラス
 */
@RestController
@RequestMapping("/error")
public class CustomErrorController implements ErrorController {

    /**
     * エラー用のJSONレスポンスの返却を行う。
     *
     * @param request
     * @return ResponseEntity<ErrorResponseModel>
     */
    @RequestMapping(produces = MediaType.ALL_VALUE)
    public ResponseEntity<ErrorResponseModel> handleError(HttpServletRequest request) {
        HttpStatus status = getStatus(request);
        ErrorResponseModel res = new ErrorResponseModel(status.getReasonPhrase());
        return ResponseEntity
            .status(status)
            .headers(httpHeaders -> httpHeaders.setContentType(MediaType.APPLICATION_JSON))
            .body(res);
    }

    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        try {
            return HttpStatus.valueOf(statusCode);
        } catch (Exception ex) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    @Deprecated
    @Override
    public String getErrorPath() {
        return null;
    }
}
