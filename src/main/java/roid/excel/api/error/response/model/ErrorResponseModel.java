package roid.excel.api.error.response.model;

public class ErrorResponseModel {

    public ErrorResponseModel(String message) {
        this.message = message;
    }

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
