package roid.excel.api.sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import roid.excel.api.sample.service.SampleService;
import roid.excel.api.sample.service.request.model.SampleRequestModel;

import java.io.IOException;
import java.util.Map;

@RestController
public class SampleController {


    @Autowired
    private SampleService sampleService;

    @RequestMapping(method = RequestMethod.GET, value = "sample-get")
    public SampleRequestModel get(SampleRequestModel sampleRequestModel) throws IOException {

        sampleService.updateSampleRequestModel(sampleRequestModel);

        return sampleRequestModel;
    }

    @RequestMapping(method = RequestMethod.POST, value = "sample-post")
    public SampleRequestModel post(SampleRequestModel sampleRequestModel) {

        sampleService.updateSampleRequestModel(sampleRequestModel);

        return sampleRequestModel;
    }
}
