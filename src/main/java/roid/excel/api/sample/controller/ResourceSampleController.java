package roid.excel.api.sample.controller;

import org.springframework.util.FileSystemUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import roid.excel.api.resource.ResourceAccessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ResourceSampleController {

    // ResourceAccessorのメンバ変数をprivate finalで宣言します。
    private final ResourceAccessor accessor;

    // コンストラクタの引数にResourceAccessorを入れると、アプリケーション起動時に
    // コンテナから自動でResourceAccessorのオブジェクトが渡されてくる（Autowired）ので、メンバ変数に設定します。
    public ResourceSampleController(ResourceAccessor accessor) {
        this.accessor = accessor;
    }


    /**
     * pathで指定したファイルが存在する場合existを返却し、
     * 存在しない場合はnothingを返却するAPI
     *
     * @param path データディレクトリからのファイルパス
     * @return Map<String, String> key：result、value: exist or nothing
     */
    // URL：http://localhost:8080/resource-get?path=/sample.txt
    @RequestMapping(method = RequestMethod.GET, value = "resource-get")
    public Map<String, String> get(@RequestParam("path") String path) throws IOException {
        // ファイルをダウンロードする一時ディレクトリを作成してください。
        Path tmpDir = Files.createTempDirectory("");
        try {
            // getFileメソッドにapp.resource.config.rootからのファイルのパスを渡すと、ファイルオブジェクトがダウンロードできます。
            // ローカル環境では、dataディレクトリからのパスを指定します。
            // ステージング環境以降では、S3のバケット名以降のパスを指定します。
            File file = this.accessor.getTemporaryFile(path, tmpDir);
            Map<String, String> res = new HashMap<>();
            if (file == null) {
                // 指定ファイルが無い場合は{"result": "nothing"} をレスポンスとして返却します。
                res.put("result", "nothing");
            } else {
                // 指定ファイルがある場合は{"result": "exist"} をレスポンスとして返却します。
                res.put("result", "exist");
            }
            return res;
        } finally {
            // 一時ディレクトリは必ず削除してください。
            FileSystemUtils.deleteRecursively(tmpDir);
        }
    }


    /**
     * srcPathで指定したファイルをdestPath指定先にコピーするAPI
     * コピーに成功した場合はsuccessを返却
     * srcPathで指定したファイルが存在しない場合や、ファイルコピーに失敗した場合はfailedを返却
     *
     * @param srcPath  データディレクトリからのファイルパス（コピー元）
     * @param destPath データディレクトリからのファイルパス（コピー先）
     * @return Map<String, String> key：result、value: success or failed
     */
    // URL：http://localhost:8080/resource-copy?srcPath=/sample.txt&destPath=/sample_copy.txt
    @RequestMapping(method = RequestMethod.GET, value = "resource-copy")
    public Map<String, String> copy(@RequestParam("srcPath") String srcPath, @RequestParam("destPath") String destPath) throws IOException {
        Path tmpDir = Files.createTempDirectory("");
        try {
            File file = accessor.getTemporaryFile(srcPath, tmpDir);
            Map<String, String> res = new HashMap<>();
            boolean result = false;
            if (file != null) {
                // 保存に成功するとtrueが返ってきて、失敗するとfalseが返ってきます。
                result = this.accessor.saveFile(file, destPath);
            }
            res.put("result", result ? "success" : "failed");
            return res;
        } finally {
            FileSystemUtils.deleteRecursively(tmpDir);
        }
    }
}