package roid.excel.api.sample.service;

import org.springframework.stereotype.Service;
import roid.excel.api.sample.service.request.model.SampleRequestModel;

@Service
public class SampleService {

    public String addOneToString(String s) {

        int i = Integer.parseInt(s);
        i++;

        return String.valueOf(i);
    }

    public String addDefToString(String s) {
        String def = "def";

        StringBuilder sb = new StringBuilder();

        sb.append(s);
        sb.append(def);

        return sb.toString();
    }

    public void updateSampleRequestModel(SampleRequestModel sampleRequestModel) {

        String param1 = sampleRequestModel.getParam1();
        String param2 = sampleRequestModel.getParam2();

        String p1 = addOneToString(param1);
        String p2 = addDefToString(param2);

        sampleRequestModel.setParam1(p1);
        sampleRequestModel.setParam2(p2);
    }
}
