package roid.excel.api.sample;

public class Example {
    public boolean devide5(int num1) {
        int ans = num1 % 5;
        if (ans == 0) {
            return true;
        }
        return false;
    }

    public void checkDevide5(int i) {

        boolean b = devide5(i);

        if (b) {
            System.out.println("割り切れる");
        } else {
            System.out.println("割り切れない");
        }
    }
}